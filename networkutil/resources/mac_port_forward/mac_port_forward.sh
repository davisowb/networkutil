#!/bin/bash

#Check if run as root
if [ "$UID" -ne "0" ] ; then
	echo "You must be root to do that!"
	exit 1
fi

if [ "$1" = "-e" ]
then
    echo Enabling port forward for ports: 53, 80
    echo "
    rdr pass inet proto tcp from any to any port 80 -> 127.0.0.1 port 9080
    rdr pass inet proto udp from any to any port 53 -> 127.0.0.1 port 9053
    " | pfctl -ef -
elif [ "$1" = "-d" ]
then
    echo Disabling port forward for ports: 53, 80
    pfctl -F all -f /etc/pf.conf
else
    echo "Valid options are -e (Enable) or -d (Disable)"
fi
