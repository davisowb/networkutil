# encoding: utf-8

import time
import logging_helper
from networkutil.sniffer import Sniffer

logging = logging_helper.setup_logging(level=logging_helper.INFO)


if __name__ == u'__main__':
    # Start sniffing thread and finish main thread.
    s = Sniffer(dev=u'en18')

    # Run for 5 seconds
    s.start()
    time.sleep(5)
    s.stop()
